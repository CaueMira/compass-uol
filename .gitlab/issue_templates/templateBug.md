# Titulo
Forma simples e direta de nomear o bug

# Descrição
Descrevcer de forma clara o que esta ocorrendo

# Ação/passos para ocorrencia
Quando e como ocorre o bug

# Frequência
Sempre, as vezes, raramente, só uma vez

# Resultado esperado
O que era esperado que o software fizesse com uma determinada ação

# Resultado obtido
O que realmente aconteceu devido ao erro

# Mensagem de erro
Qual foi a mensagem de erro exibida

# Gravidade
Quanto o bug impacta no funcionamento e uso do software

# Prioridade
Urgencia com que o bug deve ser tratado

# Anexo
print, video, logs, qualquer coisa que documente a existencia do erro
