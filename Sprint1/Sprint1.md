# **SPRINT 1**



# _Dia 1 (12/06/2023)_:
- Onboard e apresentação.
- Funcionamento de Git, Gitlab
- Como criar readme 

# _Dia 2 (13/06/2023):_
## Processos ageis

Surgiram devido a necessidade de gerenciar projetos complexos, eles possibilitam alteraçoes durante o projeto e tem uma maior comunicação com o cliente.

- Cliente pode não saber exatamente o que quer.

- Requisitos vivem mudando.

## **SCRUM**

é um modelo de processo agil que faz a entrega do produto por partes

- Possui 3 papéis

   - Projec Owner (PO) - pode ser alguem do cliente ou um representante do cliente, responsavel por passar a visão do produto

  - Equipe - Os desenvolvedores do projeto

  - Scrum Master -  especialista em scrum, ajuda o trabalho a fluir

### SPRINT

![Semantic description of image](https://caroli.org/wp-content/uploads/2016/11/scrum-cerimonies.png)*https://caroli.org/sobre-o-scrum/*

- Criar backlog

historias de usuario, definir prioridades (time e PO)
- planejar sprint

a partir das prioridades e previsão de tempo de desenvolvimento é decidido o que vai ser feito na sprint
- Executar

fazer o que foi definido no planejamento

- Revisão da sprint

ver se todas as tarefas foram executas e aprovadas pelo PO, caso não seja a tarefa folta pro backlog para entrar na proxima sprint
- Retrospectiva

Avaliar o que foi feito e possiveis melhoras que podem ser realizadas nas proximas sprints
- Implementar

fazer o incremento do que foi produzido

# _Dia 3 (14/06/2023):_
## Curso **"Início Rápido em Teste e QA"**
- carreira em testes

Area ampla em pleno crescimento no mundo todo, com boa remuneraçao e oprtunidades de crescimento

Exigente em prazos e aprendizado continuo

- Perfil do tester

Dev deve testar, mas ele pode não encontrar todos os defeitos(viés de confirmação)

Todos os envolvidos no projeto podem testar, mas tester tem conhecimento de melhores formas de testar e como prevenir esses erros

- Habilidades do tester: 
  - Motivação
  - persistencia
  - curiosidade
  -  gostar de aprender
  - perfeccionista
  - detalhista
  - resiliente
  - foco em solução
  - organização (muita informação, canais, arqivos, tarefas)
  - priorização
  - autogerenciamento
  - Comunicação
  - negociação
  - empatia
  - trabalho em equipe

<br>

- Habilidades técnicas

Gerais: SO(console, linhas de comando), Office, e-mail, mensagens instantaneas, idiomas, internet(pesquisa, contatos, fluxos, cotações), acesso remoto, segurança.

Ter conhecimento sobre o tipo de negócio que vai aplicar teste. Saber os padrões desse negócio (normas, modelos, certificações), a legislação para ele, os usos e costumes, as regras,concorrencia, saber o uso real do software (vivenciar)

Conhecimentos em tecnologia:programação, telecomunicações (meios de comunicação,, redes), infra e banco de dados(relacionais e não relacionais)

Teste e QA: Planejar os testes, analisar, modelar,implementar, executar (manual, auto, comunicar defeito, acompanhar defeitos), tecnicas de testes, praticas, processos, estratégias e ferramentas

- débito técnico

O que voce deveria saber mas ainda não sabe.

Planejar como resolver a divida, seguir o plano e rever ao longo do processo

# _Dia 4 (15/06/2023):_

## Continuação do curso (Seção 2 - **Introdução aos testes de software**)

- importancia dos testes

Bugs causam prejuizos (financeiro e imagem) e atrasos

bugs podem levar a vulnerabilidades

## fundamentos do teste (ISTQB)

- fundamento 1 - teste demonstra a presença de defeitos

o teste não pode provar que não existem defeitos, não é por que não foi encontrado que não existe

- fundamento 2 - teste exaustivo não é possivel

Tem um limite de quanto se gastar para procurar e corrigir defeitos

Não é viavel testar todas as combinações de entradas de um software

Riscos e prioridades

- fundamento 3 - teste antecipado

Começar os testes o mais breve possivel no desenvolvimento

quanto mais tarde o bug é encontrado, mais caro é pra corrigir (regra 10 de Meyers - cada etapa aumenta 10x o preço)

- fundamento 4 - agrupamento de defeitos

pequeno numero de modulos contem a maioria dos defeitos (saber onde costumam ter mais defeitos)

- fundamento 5 - paradoxo do pesticida

mesmos testes repetidas vezes podem nao encontrar novos defeitos após um tempo

testes precisam ser revisados e atualizados, nos testes escritos para investigar diferentes partes do software

- fundamento 6 - teste depende do contexto

diferentes softwares precisam de testes de amplitude e profundidades diferentes - entender os riscos do cliente -

- fundamento 7 - ilusão da ausencia de erros

de nada adianta ficar corrigindo erros se o software nao atende ao que o cliente precisa

## diferença entre teste e QA

teste avalia o produto (bugs), ja QA avalia o processo (analisam desenvolvimentos passados para melhorar o desenvolvimento futuro)

## erro, defeito e falha

pessoa comete ERRO, que gera um DEFEITO (bug), que leva a uma FALHA quando o sistema for executado. Mas nem todo defeito leva a falha

problema no código é defeito (encontrado na empresa), no sistema é falha (encontrado pelo cliente)

## IEC/ISO 25010

- Norma/padrão - requerimentos e avaliação da qualidade de software - Substituiu a ISO 9126

- Adequação funcional

Se o software cumpre seu propósito - completude (faz o que deveria fazer de forma completa) - correção (dar o resultado certo) - apropriado (dar o resultado de forma apropriada)

- Usabilidade

facilidade para o usuario

Reconhecibilidade: facilitar a reconhecer os elementos e comportamentos

Aprendizibilidade: facilitar ao usuario aprender a utilizar 

Operabilidade: facilitar uso e navegação

Proteção contra erro do usuario: Não permitir que o usuário realize ações erradas

Estética (interface do usuário): melhorar a percepção e não cansar o usuário

Acessibilidade: facilitar o acesso para todos

- Compatibilidade

Coexistência: software criado pode coexistir com outros instalados

Interoperabilidade: software criado consegue se comunicar com outros softwares

- Confiança:

Maturidade: prevenir falhas antes que aconteça, aviso prévio de algo que vai acontecer

Disponibilidade: Softwares vai estar sempre disponivel para o uso

Tolerância a falhas: perceber e compensar falhas

Recuperabilidade: recuperar de falhas e travamentos

- Eficiência

Software consegue executar sua funçao de forma eficiente e rápida

Utilizar bem os recursos dispiniveis para atender a capacidade do software

- Manutenibilidade

Facilidade de dar manuntençao ao longo da vida do software

Modularidade: manutenção em módulo não afeta os demais

Reusabilidade: Software funciona em diferentes ambientes

Analisabilidade: código facil de ler e ser analisado

Modificabilidade: código facil de ser modificados sem ter que mudar o código todo

Testabilidade: software facil e viavel de ser testado

- Portabilidade

Funciona em varios ambientes

Adaptabilidade: software se adapta ao ambiente sem precisar de interferência

Instalabilidade: facilidade de instalar e desistalar recursos

Substituibilidade: facilidade de sunstituir por outro software ou versão

- Segurança

Confidencialidade: dados não podem ficar livres para quem nao tem autorização

Integridade: apenas pessoas autorizadas podem editar dados

Não repudio: um usuario não pode ter como negar um mal uso

Responsabilidade: prestação de contas das movimentações dos usuários

Autenticidade: garantir que os dados ou transação é autentica

## testes automatizados

saber quando utilizar teste automatizados, códigos muito simples pode ser mais viavel fazer testes manuais

- deve ser criado diariamente, e rodado todo dia, para encontrar erros antecipadamente

integração continua: a cada compilaçao é executada uma série de testes automatizados, se ela for estavel ela é compilada para ambiente de testes e ser feitos ttestes mais profundos para liberar para o cliente

teste de regressão: tem a função de garantir que tudo que ja foi produzido esta funcionando e os resultados dos testes ficam visíveis para toda a equipe, quando é automatizado os testadores podem se concentrar em outros testes

# _Dia 4 (16/06/2023):_

**Discussão em Grupo: Aline Priscila Daura, Cauê Trani de Mira, Fabio Eloy Passos**

GERAR QUALIDADE DO PRODUTO:
Requisitos atendidos; Confiança; identificar defeitos; tomadas de decisão; reduzir riscos; conformidades contratuais e regulatórias
Gerar qualidade não é somente testar o software, nossa principal preocupação deve ser garantir que a solicitação do cliente está de fato sendo realizada pela aplicação.
O desafio do teste de software é encontrar a eficiência e a eficácia

Teste dinâmico: feito com o software sendo executado, é o mais utilizado, mas o mais caro, neste teste a funcionalidade é testada em tempo real.

Teste estático: Revisão do código, documentação e outros artefatos sem que o software seja executado, geralmente esse teste procura um erro de logica ou um problema com os requisitos nas documentações. 

Geralmente o testador faz um teste no software e o desenvolvedor faz a depuração do código (corrige os erros encontrados) após isso o testador faz um teste de confirmação para verificar se os erros foram resolvidos

O teste só aumenta a qualidade do software quando os defeitos são encontrados e corrigidos, e ocorre a verificação da conformidade dos requisitos funcionais e não-funcionais
Esse aumento de qualidade gera melhoria de produtividade e diminuição nos custos

PARA UMA BOA IMPLEMENTAÇÃO DE TESTES EM UM PROJETO O TESTADOR DEVE SABER:
- Requisitos do projeto/produto
- Histórias de usuário
- Requisitos não funcionais
- Especificações funcionais e ou técnicas
- Lista de navegadores que devem ser suportados
- Ambientes operacionais
- Dispositivos móveis

**O tester deve estar atento a todos os aspectos do processo*

OS 7 PRINCIPIOS:
Testes mostram a presença de erros. nunca é possível afirmar 100% que um produto não tem falhas. Apenas Podemos usar os testes para reduzir o número de defeitos não encontrados.
Testes Exaustivos são impossíveis. Não adianta investir tempo e dinheiro criando milhões de cenários, é melhor focar naqueles potencialmente mais significativos.
Teste Cedo/Antes. o custo de um erro cresce exponencialmente ao longo do processo de Desenvolvimento. Regra 10 de Meyers - cada etapa aumenta 10x o preço
Defeitos se agrupam. Princípio de Pareto no Teste de Software. Isto significa que aproximadamente 80% dos erros são usualmente encontrados em 20% dos módulos do sistema. Portanto é mais efetivo focar os testes nesses módulos.
Paradoxo do Pesticida.  Após um determinado período não adianta rodar o mesmo conjunto de testes pois não será mais encontrado erros. Revisar e atualizar regularmente os testes para adaptá-los e conseguir encontrar mais erros.
Teste depende do contexto. Dependendo do proposito ou da indústria, diferentes aplicações devem ser testadas diferentemente, já que diferentes setores têm diferentes necessidades e prioridades. 
Falácia da Ausência de erros.  Não importa o quanto seu produto é livre de erros se ele não é útil ou atende as expectativas dos usuários. 

- Teste deve ser processo independente gerido por profissionais imparciais.  
- Testar valores inválidos e inesperados além de valores esperados e válidos.  
- Testes devem ser feitos apenas em partes estáticas do software (mudanças não devem ser feitas durante o processo de testes).
- Use documentação exaustiva e compreensiva para definir os resultados esperados.

ATIVIDADES E TAREFAS DE TESTES
Planejamento do teste > Análise do teste > Modelagem do teste > Implementação do teste > Execução do teste > Conclusão do teste

![imagem](https://gitlab.com/CaueMira/compass-uol/-/raw/main/Imagem/monitoramento.jpg)

A PSICOLOGIA DO TESTE
O QA precisa ter empatia e respeito além de humildade e saber se expressar.
O QA tem que tomar cuidado com a forma que vai comunicar os problemas aos desenvolvedores, evitando o tom de crítica, também dar feedbacks positivos.

CONCLUSÃO
Ao nos reunirmos, após a leitura do material, pudemos esclarecer melhor algumas dúvidas pessoais, conversamos e percebemos a visão do outro perante o mesmo material, vimos que as nossas conclusões foram bem parecidas e concluímos que:
Para um software ter qualidade ele precisa atender n requisitos e não apenas ser feito muitos testes;
Um software bem testado e que não apresente falhas vai ser mais bem aceito pelo cliente e gerar melhor reputação com o público;
É importante que seja feito o maior número de testes possíveis, mas respeitando os prazos;
As universidades não costumam focar muito nessa área, mesmo ela representando grande melhoria na elaboração de um produto;
Ao contrário da crença comum de que os testes devem ser elaborados no fim do projeto, eles devem ser feitos o quanto antes, para ajudar no desenvolvimento correto do software, para que ele não precise ser refeito posteriormente;


# _Dia 6 (19/06/2023):_

## A piramide de testes

TOPO: testes de ponta a ponta - simula o uso real do software, como se o usuario estivesse usando, se seu software for uma API, então o usuário é quem vai consumir essa API. Esse é o tipo de teste mais demorado, trabalhoso e caro de ser criado, e tambem o que leva mais tempo para rodar.

MEIO: teste de  integração - testa as funcionalidades, a forma como as unidades interagem entre si. Mais complicados que os testes de unidade, mas mais simples que os testes ponta a ponta

BASE: teste de unidade -  teste feito na menor unidade de código testável da nossa aplicação, independente da interação dela com outras partes do nosso código. São testes simples, rapidos e baratos de se fazer

## webinar - Inteligencia artificial generativa

# _Dia 7 (20/06/2023):_

## **Curso AWS -  Job roles in the cloud**

## FUNÇÕES DA TI

## arquiteto

- De sistemas - com os lideres para criar um projeto geral, e passar a visão do cliente para os devs

- de aplicações - cria  as aplicaçoes para atender as funcionalidades necessarias

- de armazenamento -  cria a infraestrutura para armazenar dados

- de redes - cria a arquiterura da rede e da suporte se necessario

- de segurança - especifica requisitos e delega tarefas para garantir a segurança da aplicação

## administrador de sistemas

- instalação, suporte e manutenção (hardware, sistemas, rede, gerenciamento de logs, SOs, backup, controle de acesso)

## administrador de aplicações

- responsavel pelas aplicações empresariais (instalaçõa, atualização, ajustes, solução de problemas, documentação, registros)

## adm de banc de dados

- Criar, instalar e dar manutençao em banco de dados

- treinar funcionarios

- operaçoes - gerencia banco de dados; desenvolvimento - cria arquitetura; adm de dados - gerencia acesso aos dados

## adm de rede

- projetar, instalar, configurare manter LANs e WANs

## Adm de armazenamento

- Gerenciamento dos sistemas de armazenamento (instalação, confguração, substituiçao, testes e manutençaõ,backup, gerenciar capacidade)

## Adm segurança

- Instalaçõa configuraçao e gerenciamento de toda a segurança em TI

## FUNÇÕES NA NUVEM

![](https://gitlab.com/CaueMira/compass-uol/-/raw/main/Imagem/nuvens.png)

![](https://gitlab.com/CaueMira/compass-uol/-/raw/main/Imagem/Servi%C3%A7os_comuns_na_nuvem.png)



## FUNÇÕES NA NUVEM E INFRAESTRUTURA COMO CÓDIGO

- Há varias formas de gerenciar um ambiente na nuvem, pode ser feito de forma manual ou automatizada

- Linha vermelha - é como dividir as tarefas de desenvolvimento e operações. Linha alta te da mais controle sobre as açoes, quanto mais baixa a linha mais baixa a linha mais automatizado. o nivel mais baixo é a infraestrutura como codigo, genuino DevOps

# modulo 2 **AWS Cloud Practitioner Essentials**

## Computação em Nuvem

- pagamento apenas conforme o uso

- Implantação baseada na nuvem - todas as partes do aplicativo na nuvem

- Implantaçao local - recursos são implantados no local usando ferramentas de virtualização e gerenciamento de recursos.

- Implantação hibrida -  os recursos baseados na nuvem ficam conectados à infraestrutura local

Computação em nuvem tem menos despesas iniciaias, não tem gastocom manutenção de data center, não precisa saber a capacidade que ira precisar, mais rapido e maior alcance

## Amazon Elastic Compute Cloud (Amazon EC2)

### Tipos (familias)

- Uso geral - recursos equilibrados (web, serviço codigo)

- Otimizada para computação - Processadores de alto desempenho (web de alto desempenho, servidores de aplicativos de computação intensiva e servidores de jogos dedicados)

- Otimizada para memória - Processar grandes conjuntos de dados na memória, grande quantidade de dados é pré carregada antes de executar o aplicativo

- Computação acelerada - Aceleradores de hardware ( cálculos de números com vírgula flutuante, processamento de gráficos, correspondência de padrões de dados, streaming de jogos)

- Otimizada para armazenamento - grandes conjuntos de dados no armazenamento local

### Preço do EC2

- Sob demanda, apenas enquanto esta usando

- Saving plans - preço mais baixo mediante compromisso de uso consistente 1-3anos

- Instância reservada - preço mais baixo, uso previsivel, pagamento pode ser adiantado 1-3 anos

- Instância Spot - usam a capacidade de computação não utilizada do Amazon EC2 e têm uma economia de até 90%, mas a amazon pode cortar essas instancias caso necessite, voce só usa elas enquanto estao disponiveis. Portanto essa modalidade é pra ser usada em trabalhos que podem ser interrompidos e que não tem prazo de término

Hosts dedicados - servidor fisico que não vai ser compartilhado com outros clientes

### Escalabilidade

- começar apenas com os recursos que precisa naquele momento

- sistema possui redundancia para garantir que estara sempre disponivel para a demanda normal

- demanda dinamica, conforme a demanda aumente ou diminui as instancias que são utilizadas são ativadas ou desativadas, pode ser configurado o numero minimo e maximo de instancias que serão ativadas

- scaling preditivo - programa automaticamente o número de instância com base na demanda prevista.

## Elasticidade

- direciona o trafico para evitar filas nas instancias (balanceador de carga) 

- Elastic load balance - controlador regional de trafego auto escalavel

## Sistemas de mensagens e filas

- Buffer de solicitações, acoplamento flexivel, evita erro quando uma parte não esta podendo receber mais solicitaçoes ( elas ficam no buffer, que é uma fila) - Amazon Simple Queue Service (Amazon SQS)

- para isso os componentes da aplicaçao devem funcionar como microsserviços, não estando totalmente acoplados e dependentes um do outro

- Amazon Simple Notification Service (Amazon SNS) - sistema para enviar mensagens aos clientes

## outras funçõs do EC2

- AWS lambda - servless (vc nao pode ver ou acessar as estruturas subjacentes, apenas foca na sua aplicação) vc coloca seu codigo no lambda e configura um trigger pra qndo ele vai executado- serviços rapidos de até 15 min de processamento

- Amazon Elastic Container Service (Amazon ECS) - contêineres são uma maneira comum de empacotar códigos, configurações e dependências do aplicativo em um único objeto - o ECS é um sistema de gerenciamento de contêineres altamente dimensionável e de alto desempenho que permite executar e dimensionar aplicativos em contêineres na AWS.

- Amazon Elastic Kubernetes Service (Amazon EKS) - é um serviço totalmente gerenciado que você pode usar para executar o Kubernetes na AWS. O Kubernetes é um software de código aberto que permite implantar e gerenciar aplicativos em contêineres em grande escala. Ou seja, os conteiners são gerenciados para voce

-  AWS Fargate é um mecanismo de computação sem servidor para contêineres. Ele funciona com o Amazon ECS e o Amazon EKS.Com Fargate você não precisa provisionar ou gerenciar servidores. O AWS Fargate gerencia sua infraestrutura de servidor para você

# modulo 3

## infraestrutura da AWS

- Datacenters em regiões com mais necessidade de uso, são isoladas por segurança (dados não transitam entre elas, a menos que seja pedido) - cada região esta sujeita as leis do local

- escolha pela conformidade dos seus dados com as leis, proximidade dos seus clientes, disponibilidades de serviços nas regiões e preço

## zona de disponibilidade

- conjunto de datacenters, ficam longe para evitar desastres

- cada região tem pelo menos 2 zonas de disponibilidade

## pontos de presença

- CDN - amazon cloudfront - armazenar cópias em cache do seu  conteúdo (q esta em uma zona de disponibilidade) mais próximo dos seus clientes para uma entrega mais rápida. esses dados ficam armazenados em um Local de Borda

## Como provisionar recursos aws

- APIs 

- AWS Outposts - Estender a infraestrutura e os serviços AWS para seu data center local.

- AWS Management Console - interface web

- AWS Command Line Interface (AWS CLI) - permite que você controle vários serviços AWS diretamente a partir da linha de comando em uma ferramenta

- kits de desenvolvimento de software (SDKs) - facilitam o uso dos serviços AWS por uma API projetada para sua linguagem de programação ou plataforma. Os SDKs permitem que você use serviços AWS com seus aplicativos existentes ou crie aplicativos totalmente novos

- AWS Elastic Beanstalk - você fornece definições de código e configuração,  o Elastic implanta os recursos necessários para executar as seguintes tarefas - Ajustar capacidade, Balancear carga, Dimensionar de forma automática,Monitorar a integridade do aplicativo

- AWS CloudFormation - infraestrutura como código. Você pode criar um ambiente escrevendo linhas de código em vez de usar o AWS Console para provisionar recursos individualmente. o que foi criado no coud formation pode ser repetido para outros programas


# _Dia 8 (21/06/2023):_


## Módulo 4 - **conectividade e redes**

## Amazon Virtual Private Cloud (Amazon VPC)

É uma seção isolada da nuvem AWS. Nessa seção isolada, você pode executar os recursos em uma rede virtual que definir. Em uma Virtual Private Cloud (VPC), você pode organizar seus recursos em sub-redes. Uma sub-rede é uma seção de uma VPC que pode conter recursos como instâncias do Amazon EC2.

- Gateway privado - gateway é uma conexao do VPC com a internet, o Gateway privado permite acesso apenas da rede privada em q ele esta conectado.

- AWS Direct Connect - permite estabelecer uma conexão privada dedicada entre seu data center e uma VPC. A conexão privada ajuda reduzir os custos de rede e a aumentar a quantidade de largura de banda que pode trafegar pela sua rede.

## Sub-redes e lista de controle e acesso

SUb-redes ficam dentro do VCP e agrupam recursos com base em necessidades operacionais ou de segurança

- Sub-redes públicas - contêm recursos que precisam ser acessíveis ao público

- Sub-redes privadas contêm recursos que devem ser acessíveis apenas pela sua rede privada

- Lista de controle de acesso (ACL) de rede - firewall virtual que controla o tráfego de entrada e saída no nível de sub-rede. STATELESS - não lembra de nada e verifica todos os pacotes na entrada e saida

- Grupo de segurança - é um firewall virtual que controla o tráfego de entrada e saída de uma instância do Amazon EC2. Por padrão todo o tráfego de entrada é negado e todo o tráfego de saída é permitido, mas pode configurar o tráfego a ser permitido ou negado na entrada - STATEFUL - Eles se lembram de decisões anteriores tomadas para pacotes recebidos

## rede global

- Domain Name System (DNS) - A resolução de DNS é o processo de conversão de um nome de domínio para um endereço IP. 

- Amazon Route 53 - conecta solicitações de usuários à infraestrutura em execução na AWS (como instâncias do Amazon EC2 e balanceadores de carga). Ele pode direcionar os usuários para a infraestrutura fora da AWS.

Outro recurso do Route 53 é a capacidade de gerenciar os registros DNS para nomes de domínio. Você pode registrar novos nomes de domínio diretamente no Route 53, assim como, transferir registros DNS para nomes de domínio existentes gerenciados por outras empresas de registro de domínio.

<br>
<br>

## Módulo 5 - **Banco de dados**

## armazenamento de instancia e EBS

- Armazenamentos de instâncias EC2 - dados armazenados serão excluidos quando a instanciafor desativada

-  Amazon Elastic Block Store (Amazon EBS) - é um serviço que fornece volumes de armazenamento a nível de bloco, esses blocos podem ser configurados com o tamanho e tipo de volume e conectados a uma instancia EC2, o bloco armazena os dados e quando a EC2 for desligada os dadoscontinuam salvos

-  snapshot do EBS - é um backup incremental. Isso significa que o primeiro backup de um volume copia todos os dados. Nos backups subsequentes, somente os blocos de dados que foram alterados desde o snapshot mais recente são salvos. 

## Amazon Simple Storage Service (Amazon S3)

- Serviço que fornece armazenamento a nível do objeto. O Amazon S3 armazena dados como objetos em buckets.

- espaço ilimitado

- Possível definir permissões para controlar a visibilidade e acesso e usar o versionamento do Amazon S3 para rastrear alterações em seus objetos ao longo do tempo.

## Amazon Elastic File System (Amazon EFS)

- Dados em varias zonas de disponibilidade

- armazenamento de arquivos é ideal para casos de uso em que um grande número de serviços e recursos precisam acessar os mesmos dados ao mesmo tempo.

## Amazon Relational Database Service (Amazon RDS)

- banco de dados relacional - os dados são armazenados de forma que se relacionem a outras partes de dados

-  usam linguagem de consulta estruturada (SQL) para armazenar e consultar dados. Essa abordagem permite que os dados sejam armazenados de forma facilmente compreensível, consistente e dimensionável

- Amazon Relational Database Service (Amazon RDS) é um serviço que permite executar bancos de dados relacionais na nuvem AWS.

- Amazon Aurora é um banco de dados relacional de nível empresarial. É compatível com os bancos de dados relacionais MySQL e PostgreSQL.

## Amazon DynamoDB

-  Amazon DynamoDB é um serviço de banco de dados de chave-valor, bancos de dados NoSQL (não relacional). Ele oferece um desempenho de um dígito de milissegundo em qualquer scaling.

- Sem servidor, sem instalação e sem manutenção

- Escala automaticamente

## Amazon Redshift

- serviço de data warehouse que você pode usar para análise de big data, tem a capacidade de coletar dados de muitas fontes além de ajudar a entender relações e tendências em todos os seus dados.

## AWS Database Migration Service

- permite migrar bancos de dados relacionais e não relacionais e outros tipos de armazenamentos de dados.

- Devs conseguem testar app com dados de produção sem afetar usuarios

- combinar varios bancos e um unico

- envio continuo de copias de dados para fontes de destino em vez de uma unica migração

<br>
<br>

## Módulo 6 - **Segurança**

## modelo de responsabilidade compartilhada 

- responsabilidades do cliente (comumente chamadas de “segurança na nuvem”) e responsabilidades da AWS (comumente referidas como “segurança da nuvem”).

## AWS Identity and Access Management (IAM) 

- gerencie o acesso aos serviços e recursos AWS com segurança

- criar uma conta AWS pela primeira vez, você começa com uma identidade conhecida como usuário-raiz

- Não use o usuário-raiz para tarefas cotidianas. Use o usuário-raiz para criar seu primeiro usuário do IAM e atribua a ele permissões para criar outros usuários

- autenticação multifator (MFA) fornece uma camada adicional de segurança para sua conta AWS.

##  AWS Organizationspara 

- Gerenciar múltiplas contas AWS em um local central.

- Controlar as permissões das contas usando as políticas de controle de serviço (SCPs). As SCPs permitem que você coloque restrições nos serviços AWS, recursos e ações individuais de API que os usuários e funções em cada conta podem acessar.

- è possivel agrupar contas em unidades organizacionais (UO) para facilitar o gerenciamento de contas com requisitos de negócios ou segurança semelhantes

## Conformidade

- AWS Artifact - é um serviço que fornece acesso sob demanda a relatórios de segurança e conformidade da AWS e a contratos on-line selecionados.

- AWS Artifact Agreements - você pode revisar, aceitar e gerenciar contratos para uma conta individual e para todas as suas contas no AWS Organizations

- AWS Artifact Reports - fornece relatórios de conformidade por auditores terceirizados. Esses auditores testaram e verificaram se a AWS está em conformidade com diversas normas e regulamentações de segurança globais, regionais e específicas do setor

- Centro de conformidade para o cliente - você pode ler histórias de conformidade dos clientes para descobrir como as empresas de setores regulamentados resolveram vários desafios de conformidade, governança e auditoria.

## Ataque DDoS

- tentativa deliberada de tornar um site ou aplicativo indisponível para os usuários atravez de multiplas solicitações ao serviço

- AWS Shield - Protege aplicativos contra ataques DDoS

## Outros serviçoes de segurança

- AWS Key Management Service (AWS KMS) - permite que você execute operações de criptografia pelo uso de chaves de criptografia

- AWS WAF - trabalha em conjunto com o Amazon CloudFront e um balanceador de carga de aplicativo, usando uma lista de controle de acesso (ACL) da web para proteger seus recursos AWS

- Amazon Inspector - ajuda a melhorar a segurança e a conformidade dos aplicativos executando avaliações de segurança automatizadas

- Amazon GuardDuty - monitorar sua atividade de rede e conta. Você não precisa implantar ou gerenciar nenhum outro software de segurança. O GuardDuty analisa continuamente dados de várias fontes da AWS, incluindo logs de fluxo de VPC e logs de DNS. 

# Módulo 7 - Monitoramento e análise

-  Amazon CloudWatch - monitorar e gerenciar várias métricas e configurar ações de alarme de acordo com os dados dessas métricas.

- AWS CloudTrail - registra as chamadas de API realizadas na sua conta

-  AWS Trusted Advisor - inspeciona seu ambiente AWS e faz recomendações em tempo real de acordo com as práticas recomendadas da AWS

# Módulo 8 - definição de preços

- pague somento o que usar

- pague menos com reserva

- pague menos com grande volume

# Módulo 9 - Migração e inovação

##  AWS Cloud Adoption Framework (AWS CAF)

- organiza orientações em seis áreas de foco chamadas perspectivas.

- Perspectivas de negócio, pessoas e governança se concentram nas capacidades comerciais.

- perspectivas de plataforma, segurança e operações se concentram em capacidades técnicas

## Seis estratégias de migração

- Redefinição de hospedagem; Redefinição de plataforma; Refatoração/rearquitetura; Recompra; Retenção; Inativação

##  AWS Snow Family

- é uma coleção de dispositivos físicos para transporte físico de até exabytes de dados para dentro e para fora da AWS. 

# Modulo 10 - jornada para a nuvem

AWS Well-Architected Framework ajuda você a entender como projetar e operar sistemas confiáveis, seguros, eficientes e econômicos na nuvem AWS. Com ele, é possível avaliar de forma consistente suas arquiteturas em relação às melhores práticas e aos princípios de projeto e a identificar áreas para melhorias.

-  seis vantagens da computação em nuvem: Trocar despesa antecipada por despesas variáveis. Benefícios de enormes economias de escala. Parar de tentar adivinhar a capacidade.
Aumentar a velocidade e a agilidade. Parar de gastar dinheiro com execução e manutenção de data centers. Ter alcance global em minutos.

# CURSO: **GETTING STARTED WITH CLOUD ACQUISITION**

- Fundamentos da Aquisição da Nuvem: Para garantir a aquisição eficaz da nuvem, envolva todas as partes interessadas na sua organização - não apenas as suas aquisições - de forma atempada e frequente.

- Bases da Aquisição - dois tipos: diretamente dos Fornecedores de Serviços de Cloud (CSPs) (serviços de nuvem). Indiretamente dos Parceiros (trabalho para utilizar serviços de nuvem ou soluções criadas em infraestrutura de CSP)

- Aspectos Principais da Compra: O preço da nuvem é variável. Deverá avaliar a segurança e resiliência da infraestrutura física da AWS. É importante compreender a soberania de dados e residência de dados. Qualquer análise sobre o impacto climático de um centro de dados deve considerar a utilização de recursos e a eficiência energética. Deve considerar a administração da nuvem para operacionalizar com sucesso o seu ambiente.Os termos e condições da nuvem são desenvolvidos para refletir como um modelo de serviços em nuvem funciona 

- Torná-lo real

-Trabalhar com Parceiros.

# _Dia 9 (22/06/2023_):

## CURSO **AWS BILLING AND COST MANAGEMENT**

- Permite acompanhar e prever seus custos

- Dashboard  - mostra seus gastos em graficos, pode ser filtrado de diferentes maneiras e tambem mostrar gastos individuais com cada serviço

- Bill page - mostra a conta (como uma fatura) do mês anterior

- Cost explorer - forma de visualizar os gastos que ja teve em grafico

- AWS budgets -  projeta gastos futuros utilizando o Cost explorer

## CURSO **AWS WELL-ARCHITECTED**

- O AWS Well-Architected Framework - ajuda arquitetos de nuvem a construir infraestruturas seguras, resilientes, eficientes e de alta performance para aplicações e workloads


- O pilar de excelência operacional 

Execução e monitoramento sistemas e na melhoria contínua de processos e procedimentos - automação de alterações, reação a eventos e definição de padrões para gerenciar as operações diárias.

- O pilar de segurança

Proteção de informações e sistemas - confidencialidade e integridade de dados, gerenciamento de permissões de usuário e estabelecimento de controles para detectar eventos de segurança.

- O pilar de confiabilidade

Recuperação rápida de falhas em atender demandas - projeto de sistemas distribuídos, planejamento de recuperação e requisitos adaptação a mudanças.

- O pilar da eficiência de performance

Alocação estruturada e simplificada de recursos de TI e computação - seleção dos tipos e tamanhos certos dos recursos otimizados para os requisitos de workload, monitoramento de performance e manutenção da eficiência à medida que as necessidades comerciais evoluem.

- O pilar de otimização de custo

Evitar custos desnecessários - compreensão dos gastos ao longo do tempo e controle da alocação de fundos, seleção do tipo e quantidade certa de recursos e dimensionamento para atender às necessidades de negócios sem gastos excessivos.

- A revisão do Well-Architected

Minimizar os impactos ambientais da execução de workloads em nuvem - modelo de responsabilidade compartilhada para sustentabilidade, compreensão do impacto e maximização da utilização para minimizar os recursos necessários e reduzir os impactos posteriores. 

- A AWS Well-Architected Tool

## CURSO **AWS FOUNDATIONS: GETTING STARTED WITH THE AWS CLOUD ESSENTIALS**

Objetivos do curso:

- descrever a proposição de valor da Nuvem AWS
- descrever a infraestrutura global básica da nuvem
- descrever e diferenciar os domínios de serviço da AWS
- explicar o modelo de responsabilidade compartilhada
- descrever a definição de preço da AWS


O que é computação em nuvem?
- infraestrutura global da AWS
- computação
- armazenamento
- banco de dados
- redes
- segurança
- definição de preço

Grande parte do que foi visto aqui ja esta no resumo do curso2

## Curso **FUNDAMENTOS DA AWS: PROTEGENDO SUA NUVEM**

- Saber separar o que é responsabilidade do cliente e o que é da AWS

## princípios de segurança

- Privilegio mínimo
- Rastreabilidade
- Proteger todas as camadas da rede
- Automatizar a segurança (infraestrutura como código)
- Criptogrfar dados em transito e em repouso
- Estar preparado para falhas de segurança (ter um plano de como agir e Backup)
- Minimizar os possiveis locais de ataque

## camadas de proteção do VCP

- externamente - proteção na sub-rede
- intermediario - ACLs na rede
- internamente - security group

## 6 conceitos para aumentar a segurança

- Autenticação - QUEM
- Autorização - O QUE pode
- Monitoramento - QUANTO fez
- Auditoria - O QUE foi feito
- Criptografia
- Caminho de dados- controle da rede

# _DIA 10 (23/06/23)  APRESENTAÇÃO_

- O que funcionou?



- O que precisa mudar?



- Quais conteúdos teve mais dificuldade?


